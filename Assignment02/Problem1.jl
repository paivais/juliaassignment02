### A Pluto.jl notebook ###
# v0.19.3

using Markdown
using InteractiveUtils

# ╔═╡ 97337596-8baa-4b4a-bfe3-9715909fa9ef
using CSV

# ╔═╡ ddfea57c-544c-4f7c-8946-da3297be4d9e
using DataFrames

# ╔═╡ cfe83f8e-7fbd-46cc-afd6-560c253081df
using Plots

# ╔═╡ 60d7192e-88c2-4509-bbba-2892dbe2338b
using Statistics

# ╔═╡ d042e26c-7ed3-41c8-b252-81477537cab7
using StatsBase

# ╔═╡ be4e6c41-7467-44ca-8628-ab45666d7c1f
using Flux

# ╔═╡ 51067ea1-048a-4dca-bf14-581e7f1fa4ba
using RDatasets

# ╔═╡ 428c6765-077f-4c95-b6a6-3d6b9e9c21bb
using DelimitedFiles

# ╔═╡ 8158ed7f-7167-4198-9a03-b7b520b58937
using Parameters: @with_kw

# ╔═╡ 0e4af7dd-dd4a-4a0a-900b-e973c1a70aeb
using PlutoUI

# ╔═╡ 64f5db36-1842-44fb-ab2a-4a8b656ef808
using Flux: train!

# ╔═╡ 399cbf7c-b36d-4166-bc32-f2f5dfbef4ab
using Flux: gradient

# ╔═╡ 23bc2f9b-7c97-4eb4-8616-23623dad4594
using Flux.Optimise: update!

# ╔═╡ 674aa82f-db63-4cb6-865b-1b38c829a777
using Flux: onehotbatch, onecold, crossentropy

# ╔═╡ 4668976a-d058-4873-b053-c86f44908470
using Flux: @epochs

# ╔═╡ bfcbe310-0e9e-4343-93ab-2fce5867a10f


# ╔═╡ a0dbe74f-c9e6-495d-b79c-5a5bb06a4301
# ╠═╡ disabled = true
#=╠═╡
begin
	begin
		begin
			#imports
			using CSV, Dat
  ╠═╡ =#

# ╔═╡ 365b6309-a500-4c85-98ff-be528febfe43
# ╠═╡ disabled = true
#=╠═╡
aFrames, Plots, Statist
  ╠═╡ =#

# ╔═╡ 04c28207-9136-43e4-bd2a-43ee67b215a4
# ╠═╡ disabled = true
#=╠═╡
ics
			
			using StatsBase, Flux
  ╠═╡ =#

# ╔═╡ 12f5160d-1922-4937-8ded-c0d5ee409aca
# ╠═╡ disabled = true
#=╠═╡
kdown
			
			using RDatasets
  ╠═╡ =#

# ╔═╡ 164057af-2712-4c96-aa52-986b1be14a9a
# ╠═╡ disabled = true
#=╠═╡
, DelimitedFile
  ╠═╡ =#

# ╔═╡ 7212e902-b5df-4a9a-a09d-bf6f4bc7cb4d
# ╠═╡ disabled = true
#=╠═╡
s
			
			using Param
  ╠═╡ =#

# ╔═╡ 441aef0a-08f4-46ac-b02e-09856fd5aece
# ╠═╡ disabled = true
#=╠═╡
eters: @with_kw
  ╠═╡ =#

# ╔═╡ e768a6d2-b02b-47df-816e-5f35b95eb2f4
# ╠═╡ disabled = true
#=╠═╡
using PlutoUI
			
			using
  ╠═╡ =#

# ╔═╡ e5b080f7-927b-439c-9261-cfee3bb144a4
# ╠═╡ disabled = true
#=╠═╡
Flux: train!
			using Flux: gradient
			using F
  ╠═╡ =#

# ╔═╡ 48bb95a6-6322-4830-a111-dc21bad011d9
# ╠═╡ disabled = true
#=╠═╡
lux.Optimise: updat
  ╠═╡ =#

# ╔═╡ 4b262cf3-3ef7-458e-bfae-d924310d013f


# ╔═╡ 6714e3a5-a46a-4a5b-bb21-9f303a56356d
#getting dataset
real_estate_df = CSV.read("C:/Users/ivanp/Documents/Projects/Assignment02/Real estate.csv", DataFrame) 


# ╔═╡ 690e3f2d-830a-4454-8e69-b9b73bcea761


# ╔═╡ 3d2c41d4-b595-4f50-b2bc-0ca867eeedb6
begin
	rename!(real_estate_df, Symbol.(replace.(string.(names(real_estate_df)), Ref(r"\[m\]"=>"_"))))
	rename!(real_estate_df, Symbol.(replace.(string.(names(real_estate_df)), Ref(r"\[s\]"=>"_"))))
	rename!(real_estate_df, Symbol.(replace.(string.(names(real_estate_df)), Ref(r"\s"=>"_"))))
end

# ╔═╡ 557f0ac9-2620-4101-b82e-67f7cad8be32


# ╔═╡ 043ed514-0b3e-46d9-8ff7-a3a0ef27ea43
size(real_estate_df)

# ╔═╡ 736b7c58-5550-4d30-a758-960335a432db
with_terminal() do
	for col_name in names(real_estate_df)
		println(col_name) 
	end
end

# ╔═╡ a1bf608f-d122-4281-a292-40123f777056
with_terminal() do
	describe(real_estate_df[!, :X6_longitude])
end

# ╔═╡ cc85b98e-d76b-4096-b0b2-e08c54b19dfa
X = real_estate_df[:, 2:5]

# ╔═╡ 3b914b12-1f00-487a-bb80-3c1f112be20d
X_m = Matrix(X)

# ╔═╡ 661f6b97-cd3d-4203-88db-10c9e51ac0f2
Y = real_estate_df[:, 7]

# ╔═╡ 95a8686c-59f2-49b8-9cfd-8d6669e8630a
Y_m = Vector(Y)

# ╔═╡ c19c4921-3949-4ac9-b95f-a7b63cfec603
training_size = 0.7

# ╔═╡ a9cdab62-b9f3-4c1d-9c94-4eacb973db16
data_size = size(X_m)[2]

# ╔═╡ 71058fcd-80e0-4258-a41f-314792768ae8
training_index = trunc(Int, training_size * data_size)

# ╔═╡ 4496237d-841b-4417-a0bb-7309c01a4176
X_train = X_m[1:training_index, :]

# ╔═╡ 84f10de1-c685-4598-8872-62f61b66bf54
X_test = X_m[training_index+1:end, :]

# ╔═╡ 1cde0083-4b80-48cb-b944-bc8d0ef797fa
Y_train = Y_m[1:training_index]

# ╔═╡ adfd71c7-618b-40f8-96f5-6ccab6706c61
Y_test = Y_m[training_index+1:end]

# ╔═╡ 1606ca6d-417d-450c-a01a-89c09fc302af
function processed_data(args)

    x = (x .- mean(x, dims = 2)) ./ std(x, dims = 2)

    train_data = (X_train, Y_train)
    test_data = (X_test, Y_test)

    return train_data,test_data
end

# ╔═╡ b92b4580-d9db-4daf-8652-735df185fd85
begin
	plot(X_train, Y_train, title="linear regression", label="train data", seriestype = :scatter)
	
    plot!(X_test, Y_test, label="test data", seriestype = :scatter)
end

# ╔═╡ a687c916-bde6-446d-963a-20893f8f54e9
mod(x) = w*x + b

# ╔═╡ 8c382fa9-081a-4c2f-ae41-98c944e9c06e
model =
  Dense(95,5)

# ╔═╡ 3ca688b6-a1b1-4f45-bb26-6b579d0b0424
ps = Flux.params(model)

# ╔═╡ 3be56345-1c52-4f9a-a19e-a6f61a1d1ec3
function loss(x, y)
  ŷ = model(x)
  sum((y .- ŷ).^2)
end

# ╔═╡ 2f554de6-c030-4abf-9cd8-45cd9393006c
optimiser = Descent()

# ╔═╡ cee8a7cc-1cd4-40f5-a931-8e6f5c638ca7
predict(x) = model(x)

# ╔═╡ 6c174954-fe04-4546-984f-4f1bde71adc6
pred_0 = predict(X_test)

# ╔═╡ d817ee98-53ff-46ee-bc2f-38cb065db37f
loss_0 = loss(predict(X_test), Y_test)

# ╔═╡ 00a7c476-ee96-44ec-9348-a202151c285c
meansquarederror(ŷ, y) = sum((ŷ .- y).^2)/size(y, 2)

# ╔═╡ 37b7ec4b-ef75-4dae-ab16-72e572ff7670
function get_loss(features::Matrix{Float64}, outcome::Vector{Float64}, weights::Vector{Float64})::Float64
	m = size(features)[1]
	hypothesis = features * weights
	loss = hypothesis - outcome
	cost = (1/(2m)) * (loss' * loss)
	return cost
end

# ╔═╡ 963053d6-b380-460a-a255-14afbd68490a
function the_scaling_params(init_features::Matrix{Float64})::Tuple{Matrix{Float64}, Matrix{Float64}}
	feature_mean = mean(init_features, dims=2)
	f_dev = std(init_features, dims=2)
	return (feature_mean, f_dev)
end

# ╔═╡ dd31bd10-db83-4553-a2ac-f560b423194a
function scale_features(features::Matrix{Float64}, sc_params::Tuple{Matrix{Float64}, Matrix{Float64}})::Matrix{Float64}
	normalised_features = (features .- sc_params[1]) ./ sc_params[2]
end

# ╔═╡ 095b98fb-d054-4fab-9b04-77d517ff7db0
scaling_params = the_scaling_params(X_train)

# ╔═╡ 6f86c9c6-ca2e-453d-811e-843200ce7637
scaling_params[1]

# ╔═╡ 62262b4d-da73-472d-92c8-00893b47b0f0
last(scaling_params)

# ╔═╡ 66497025-c1ab-4dae-8c39-be262d07a161
scaled_training_features = scale_features(X_train, scaling_params)

# ╔═╡ c79744ae-f775-4640-b567-dc29b831a7eb
scaled_testing_features = scale_features(X_test, scaling_params)

# ╔═╡ 29ef4120-5ecd-4ab1-af87-014f5dad8ed3
function train_model(features::Matrix{Float64}, outcome::Vector{Float64}, alpha::Float64, n_iter::Int64)::Tuple{Vector{Float64}, Vector{Float64}}
	total_entry_count = length(outcome)
	aug_features = hcat(ones(total_entry_count, 1), features)
	feature_count = size(aug_features)[2]
	weights = zeros(feature_count)
	loss_vals = zeros(n_iter)

	for i in range(1, stop=n_iter)
		pred = aug_features * weights
		loss_vals[i] = get_loss(aug_features, outcome, weights)
		weights = weights - ((alpha/total_entry_count) * aug_features') * (pred - outcome)
	end
	
	return (weights, loss_vals)
end

# ╔═╡ 2e186423-7eef-4e0c-ac35-769e4ebc1ae0
data = zip(X_train, Y_train)

# ╔═╡ 1c723308-a869-4325-a608-7347fd09f068
Flux.train!(loss, params(w, b), data, opt)

# ╔═╡ 670c1b9b-f3a9-40cc-bd85-49e56ddbe3f2
pred_1 = model(X_test)

# ╔═╡ 8f5fa8a4-7b75-4214-a2b7-8b80509e1ddd
begin
	plot(X_train', Y_train', title="linear regression", label="train data", seriestype = :scatter)
    plot!(X_test', Y_test', label="test data", seriestype = :scatter)
end

# ╔═╡ 9de58404-43c4-4686-872f-727ba31de675
weights_tr_errors = train_model(scaled_training_features, Y_train, 0.6,700)

# ╔═╡ 25747ad0-8de8-43e2-97be-c3f98311d57a
plot(weights_tr_errors[2],
	label="Cost",
	ylabel="Cost",
	xlabel="Number of Iteration", 
	title="Cost Per Iteration")

# ╔═╡ e79af128-0063-4da5-b66d-eb2965516c91
function get_predictions(features::Matrix{Float64}, weights::Vector{Float64})::Vector{Float64}
	total_entry_count = size(features)[1]
	aug_features = hcat(ones(total_entry_count, 1), features)
	preds = aug_features * weights
	return preds
end

# ╔═╡ d79a5753-60e2-4eb8-ab7d-e6a4f3355039
function root_mean_square_error(actual_outcome::Vector{Float64}, predicted_outcome::Vector{Float64})::Float64
	errors = predicted_outcome - actual_outcome
	squared_errors = errors .^ 2
	mean_squared_errors = mean(squared_errors)
	rmse = sqrt(mean_squared_errors)
	return rmse
end

# ╔═╡ 10eb0741-a94b-4656-9437-773d1e56f2f0
training_predictions = get_predictions(scaled_training_features, weights_tr_errors[1])

# ╔═╡ 93ab293c-d029-490c-9ddc-8ef2f0b19bea
testing_predictions = get_predictions(scaled_testing_features,weights_tr_errors[1])

# ╔═╡ 5cddd967-5082-4506-b71e-cdb8d4e8ac34
root_mean_square_error(Y_train, training_predictions)

# ╔═╡ fce615a2-0884-4cd7-91ee-2f4028a086eb
root_mean_square_error(Y_test, testing_predictions)

# ╔═╡ d0c97b20-42d6-4316-8a9b-2bd637abc11b
function ridgeReg(Y, X, γ, λ = 0)
    (T, L) = (size(X, 1), size(X, 2))

    b_ls = X \ Y                    

    Q = X'X / T
    c = X'Y / T                      

    b = Variable(K)              
    L1 = quadform(b, Q)            
    L2 = dot(c, b)                 
    L3 = norm(b, 1)                
    L4 = sumsquares(b)            

    if λ > 0
        Sol = minimize(L1 - 2 * L2 + γ * L3 + λ * L4)      
    else
        Sol = minimize(L1 - 2 * L2 + γ * L3)               
    end
    solve!(Sol, SCS.Optimizer; silent_solver = true)
    Sol.status == Convex.MOI.OPTIMAL ? b_i = vec(evaluate(b)) : b_i = NaN

    return b_i, b_ls
end

# ╔═╡ d8b955c1-098e-479e-b305-a3b26d84ed40
L = size(X, 2)

# ╔═╡ 99286307-ae56-4658-915e-cd5255787152
γ = 0.25

# ╔═╡ cce6d258-0fb5-420c-8894-f37dacff9d60
using InteractiveUtils

# ╔═╡ 6b319ee5-573b-491b-9fd7-8c7c17feb318
# ╠═╡ disabled = true
#=╠═╡
using InteractiveUtils ,Mar
  ╠═╡ =#

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═bfcbe310-0e9e-4343-93ab-2fce5867a10f
# ╠═a0dbe74f-c9e6-495d-b79c-5a5bb06a4301
# ╠═365b6309-a500-4c85-98ff-be528febfe43
# ╠═04c28207-9136-43e4-bd2a-43ee67b215a4
# ╠═6b319ee5-573b-491b-9fd7-8c7c17feb318
# ╠═12f5160d-1922-4937-8ded-c0d5ee409aca
# ╠═164057af-2712-4c96-aa52-986b1be14a9a
# ╠═7212e902-b5df-4a9a-a09d-bf6f4bc7cb4d
# ╠═441aef0a-08f4-46ac-b02e-09856fd5aece
# ╠═e768a6d2-b02b-47df-816e-5f35b95eb2f4
# ╠═e5b080f7-927b-439c-9261-cfee3bb144a4
# ╠═48bb95a6-6322-4830-a111-dc21bad011d9
# ╠═97337596-8baa-4b4a-bfe3-9715909fa9ef
# ╠═ddfea57c-544c-4f7c-8946-da3297be4d9e
# ╠═cfe83f8e-7fbd-46cc-afd6-560c253081df
# ╠═60d7192e-88c2-4509-bbba-2892dbe2338b
# ╠═d042e26c-7ed3-41c8-b252-81477537cab7
# ╠═be4e6c41-7467-44ca-8628-ab45666d7c1f
# ╠═cce6d258-0fb5-420c-8894-f37dacff9d60
# ╠═51067ea1-048a-4dca-bf14-581e7f1fa4ba
# ╠═428c6765-077f-4c95-b6a6-3d6b9e9c21bb
# ╠═8158ed7f-7167-4198-9a03-b7b520b58937
# ╠═0e4af7dd-dd4a-4a0a-900b-e973c1a70aeb
# ╠═64f5db36-1842-44fb-ab2a-4a8b656ef808
# ╠═399cbf7c-b36d-4166-bc32-f2f5dfbef4ab
# ╠═23bc2f9b-7c97-4eb4-8616-23623dad4594
# ╠═674aa82f-db63-4cb6-865b-1b38c829a777
# ╠═4668976a-d058-4873-b053-c86f44908470
# ╠═4b262cf3-3ef7-458e-bfae-d924310d013f
# ╠═6714e3a5-a46a-4a5b-bb21-9f303a56356d
# ╠═690e3f2d-830a-4454-8e69-b9b73bcea761
# ╠═3d2c41d4-b595-4f50-b2bc-0ca867eeedb6
# ╠═557f0ac9-2620-4101-b82e-67f7cad8be32
# ╠═043ed514-0b3e-46d9-8ff7-a3a0ef27ea43
# ╠═736b7c58-5550-4d30-a758-960335a432db
# ╠═a1bf608f-d122-4281-a292-40123f777056
# ╠═cc85b98e-d76b-4096-b0b2-e08c54b19dfa
# ╠═3b914b12-1f00-487a-bb80-3c1f112be20d
# ╠═661f6b97-cd3d-4203-88db-10c9e51ac0f2
# ╠═95a8686c-59f2-49b8-9cfd-8d6669e8630a
# ╠═c19c4921-3949-4ac9-b95f-a7b63cfec603
# ╠═a9cdab62-b9f3-4c1d-9c94-4eacb973db16
# ╠═71058fcd-80e0-4258-a41f-314792768ae8
# ╠═4496237d-841b-4417-a0bb-7309c01a4176
# ╠═84f10de1-c685-4598-8872-62f61b66bf54
# ╠═1cde0083-4b80-48cb-b944-bc8d0ef797fa
# ╠═adfd71c7-618b-40f8-96f5-6ccab6706c61
# ╠═1606ca6d-417d-450c-a01a-89c09fc302af
# ╠═b92b4580-d9db-4daf-8652-735df185fd85
# ╠═a687c916-bde6-446d-963a-20893f8f54e9
# ╠═8c382fa9-081a-4c2f-ae41-98c944e9c06e
# ╠═3ca688b6-a1b1-4f45-bb26-6b579d0b0424
# ╠═3be56345-1c52-4f9a-a19e-a6f61a1d1ec3
# ╠═2f554de6-c030-4abf-9cd8-45cd9393006c
# ╠═cee8a7cc-1cd4-40f5-a931-8e6f5c638ca7
# ╠═6c174954-fe04-4546-984f-4f1bde71adc6
# ╠═d817ee98-53ff-46ee-bc2f-38cb065db37f
# ╠═00a7c476-ee96-44ec-9348-a202151c285c
# ╠═37b7ec4b-ef75-4dae-ab16-72e572ff7670
# ╠═963053d6-b380-460a-a255-14afbd68490a
# ╠═dd31bd10-db83-4553-a2ac-f560b423194a
# ╠═095b98fb-d054-4fab-9b04-77d517ff7db0
# ╠═6f86c9c6-ca2e-453d-811e-843200ce7637
# ╠═62262b4d-da73-472d-92c8-00893b47b0f0
# ╠═66497025-c1ab-4dae-8c39-be262d07a161
# ╠═c79744ae-f775-4640-b567-dc29b831a7eb
# ╠═29ef4120-5ecd-4ab1-af87-014f5dad8ed3
# ╠═2e186423-7eef-4e0c-ac35-769e4ebc1ae0
# ╠═1c723308-a869-4325-a608-7347fd09f068
# ╠═670c1b9b-f3a9-40cc-bd85-49e56ddbe3f2
# ╠═8f5fa8a4-7b75-4214-a2b7-8b80509e1ddd
# ╠═9de58404-43c4-4686-872f-727ba31de675
# ╠═25747ad0-8de8-43e2-97be-c3f98311d57a
# ╠═e79af128-0063-4da5-b66d-eb2965516c91
# ╠═d79a5753-60e2-4eb8-ab7d-e6a4f3355039
# ╠═10eb0741-a94b-4656-9437-773d1e56f2f0
# ╠═93ab293c-d029-490c-9ddc-8ef2f0b19bea
# ╠═5cddd967-5082-4506-b71e-cdb8d4e8ac34
# ╠═fce615a2-0884-4cd7-91ee-2f4028a086eb
# ╠═d0c97b20-42d6-4316-8a9b-2bd637abc11b
# ╠═d8b955c1-098e-479e-b305-a3b26d84ed40
# ╠═99286307-ae56-4658-915e-cd5255787152
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
