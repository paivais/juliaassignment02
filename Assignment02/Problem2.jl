### A Pluto.jl notebook ###
# v0.19.3

using Markdown
using InteractiveUtils

# ╔═╡ 8b1505c8-48d7-48a4-8fa4-dbc16bb7399b
using Markdown

# ╔═╡ 879fa0a8-fd2d-40b9-8dd5-6fea12c993d5
using Images

# ╔═╡ bd6a26cc-5967-4a35-8517-6aaa704bcda6
using ImageView

# ╔═╡ 720dd163-60ab-46b2-b8ed-8a75045e8c4b
using TestImages

# ╔═╡ a01ce91c-5474-4345-8cb4-c195be2c56f3
using FileIO

# ╔═╡ dbcfc74f-5268-42a1-a99b-ff6cb9631baf
using PlutoUI

# ╔═╡ 5c7becfb-2f5a-4f2e-b3cd-0f7549402987
using Flux

# ╔═╡ a3967197-cea6-4f57-b885-3a66db649767
using Flux: Data.DataLoader

# ╔═╡ faf90ddc-8f7f-4bb1-bc29-66755089369b
using Flux: gradient

# ╔═╡ fca0a38c-b4de-4d38-9f68-fa2fab2dccf8
using Flux: @epochs

# ╔═╡ 635e7dc6-58eb-49e7-b04d-488fa77fc7b0
using Statistics

# ╔═╡ a26c4974-f6bf-43b9-a935-7081a8009626
using Plots

# ╔═╡ c9cea1a4-0702-4626-a040-b74c70a6704b
using Flux: onehotbatch, onecold, crossentropy, throttle, logitcrossentropy

# ╔═╡ b2c08ab8-2c05-4d9d-8c6d-f46a1e0bbc78
using InteractiveUtils

# ╔═╡ 258bbe86-288b-46ce-b80f-3e5ea43c26c6
using Base.Iterators: repeated, partition

# ╔═╡ 9259deac-3d59-43c2-9942-bf66f046c301
using BSON

# ╔═╡ 82aaca93-7bc2-4975-9812-792792a5dbc9
using Printf

# ╔═╡ e4945877-3e55-4fa4-a3b8-c03b2301d66a
using Parameters: @with_kw

# ╔═╡ 76c96c8d-6a1c-4932-b2ed-1b3138320c77


# ╔═╡ 456fd5d0-6516-45a2-8640-49bce711cca2


# ╔═╡ ebf31409-e3c4-402a-b447-aa96683a9e83
function load_images(path::String)
    img_paths = readdir(path)
    imgs = []
    for img_path in img_paths
        push!(imgs, load(string(path,img_path)))
    end
    return imgs
end

# ╔═╡ 3625f053-4498-4c0d-9241-5d824bb06044
test_image1 = [load(img) for img in readdir("C:/Users/ivanp/Documents/Projects/Assignment02/chest_xray/test/NORMAL", join = true)]

# ╔═╡ 141c330d-fa0c-481f-a111-916c529cd3af
test_image2 = [load(img) for img in readdir("C:/Users/ivanp/Documents/Projects/Assignment02/chest_xray/test/PNEUMONIA", join = true)]

# ╔═╡ 2e5f645b-977f-477b-a0fa-7c850202a45d
train_image2 = [load(img) for img in readdir("C:/Users/ivanp/Documents/Projects/Assignment02/chest_xray/train/NORMAL", join = true)]

# ╔═╡ 2437042e-a8ee-44d1-a422-70cf053dae3f
train_normal_img = load("C:/Users/ivanp/Documents/Projects/Assignment02chest_xray/train/NORMAL/IM-0115-0001.jpeg")

# ╔═╡ 36bf8b93-124b-42b1-ac19-3b5f0bd4ec7d
train_2imgs = [load(img) for img in readdir("C:/Users/ivanp/Documents/Projects/Assignment02/chest_xray/train/PNEUMONIA", join = true)]

# ╔═╡ ac9a7a7b-1b79-47f0-bc10-10d795fb6a7f
train_pneumonia_img = load("C:/Users/ivanp/Documents/Projects/Assignment02chest_xray/train/PNEUMONIA/person1_bacteria_1.jpeg")

# ╔═╡ f9ce70ba-760f-4bc1-8590-da92ebd9c7a4
val_image1 = [load(img) for img in readdir("C:/Users/ivanp/Documents/Projects/Assignment02/chest_xray/val/NORMAL", join = true)]

# ╔═╡ 8a90a9ab-2b00-4b6c-8ecb-27b7065484c5
val_image2 = [load(img) for img in readdir("C:/Users/ivanp/Documents/Projects/Assignment02/chest_xray/val/PNEUMONIA", join = true)]

# ╔═╡ f22801f2-29e3-45ab-bc49-381ac2baa7c3
size(test_image1)

# ╔═╡ 4d9a80d2-5226-4001-9de1-51b16fd3c47d
size(test_image2)

# ╔═╡ 26269127-0100-48bb-a0bb-63f9c2e8a20c
size(train_image1)

# ╔═╡ a9753dc0-d195-4fb6-b90a-4bda5071c8d1
size(train_2imgs)

# ╔═╡ 29614b02-f1eb-418a-8a39-2ad4f713dfa7
size(val_image1)

# ╔═╡ 4166ad83-f0af-446b-8631-7ba7aea93876
size(val_image2)

# ╔═╡ 6e1add3f-73ce-42d1-aa5b-be0eaa00140b
df(x) = gradient(f, x)[1]

# ╔═╡ cc632f9b-09ac-432c-8857-ddaa360c571a
df(5)

# ╔═╡ 132865f6-5f64-4b43-a837-58d4b6f0b9ab
myloss(W, b, x) = sum(W * x .+ b)

# ╔═╡ c0e1e252-354d-4085-9972-c7b46a3271e4
m = Dense(10, 5)

# ╔═╡ 26d3e0d5-65b7-4c3d-98ac-7f5f800b9ceb
function make_batch(X, Y, idxs)
    Xbatch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        Xbatch[:, :, :, i] = Float32.(X[idxs[i]])
    end
	
    Ybatch = onehotbatch(Y[idxs], 0:9)
    return (Xbatch, Ybatch)
end

# ╔═╡ cf5de1ac-c42c-425c-b6d9-abee5236f531


# ╔═╡ d4a9f0d1-19cf-4028-82d4-5c9ddece714e
Array{Tuple{Array{Float32,4},Flux.OneHotMatrix{Array{Flux.OneHotVector,1}}},1}

# ╔═╡ c1ba2839-08e6-45bb-ad40-cda70ebcb601
model = Chain(
    Conv((3, 3), 1=>16, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
    Conv((3, 3), 16=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
    Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
	Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
	Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
    x -> reshape(x, :, size(x, 4)),
    Dense(288, 10),
    softmax,
)

# ╔═╡ 421063fe-0e8c-4ab5-a500-f1852b5a7bc7
function loss(x, y)
    x_aug = x .+ 0.1f0*gpu(randn(eltype(x), size(x)))

    y_hat = model(x_aug)
    return crossentropy(y_hat, y)
end

# ╔═╡ ba5d297e-da24-477c-8c20-1f3c9283934e


# ╔═╡ dea0fd8e-5819-4ac7-9bb0-fcdcf5b9f006
accuracy(x, y) = mean(onecold(model(x)) .== onecold(y))

# ╔═╡ 5bf4a473-3291-479a-943a-db5df2ad5a63
opt = ADAM(0.001)

# ╔═╡ 453bcea6-5715-42a7-b943-27164f61c9b6


# ╔═╡ f5776823-9ff5-469f-b4a3-0e090b023966


# ╔═╡ bbd25a8f-4dcc-4615-a230-8c93c70e0746
begin
	augment(x) = x .+ gpu(0.1f0*randn(eltype(x), size(x)))
    paramvec(m) = hcat(map(p->reshape(p, :), params(m))...)    
    anynan(x) = any(isnan.(x))
    accuracy(x, y, model) = mean(onecold(cpu(model(x))) .== onecold(cpu(y)))
end

# ╔═╡ bad045e3-08ee-44d0-9f5a-f43839485365


# ╔═╡ f23a69b8-0922-42dc-91e0-e0190b8e72b8


# ╔═╡ 8f444ef5-4ef1-4efd-a2df-7b33b86e8650
function sigmoid(z)
1 ./ (1 .+ exp.(.-z))
end

# ╔═╡ 968826ae-3c59-4950-9d50-ab25ad275ff6


# ╔═╡ e520135c-0b04-473a-a42a-90817086f30d
function transform_features(xMatrix, u, o)
       XNorm = (xMatrix .- u) ./ o
       return X_norm
       end

# ╔═╡ 465d03fc-14d7-4d1d-8e17-38637df4656e


# ╔═╡ f665b4c1-b6fb-4119-a560-2799a27a3138
begin
	W = randn(3, 5)
    b = zeros(3)
    x = rand(5)
end

# ╔═╡ 1831407d-0ad3-4b68-a58b-7f0297e66dfc


# ╔═╡ 1a397da2-0994-40c3-a795-a7268c750251
gradient(myloss, W, b, x)

# ╔═╡ 657346a0-5650-4031-b6f6-da4aafeb43df
size(train_set[1][1]) # Float32

# ╔═╡ 320dc020-61e9-45eb-9993-7080a206196f
size(train_set[1][2])

# ╔═╡ 96e42750-7cbf-477c-8041-0d64ab6c63f7
train_set[1][2][:,1]

# ╔═╡ 2076fcc1-d5d7-4830-befb-46ccbe13b8c2
model(train_set[1][1])

# ╔═╡ 988e000a-3ab8-4095-8854-a13071d0e0bf
begin
	best_acc = 0.0
    last_improvement = 0
    for epoch_idx in 1:100
		global best_acc, last_improvement        
        Flux.train!(loss, params(model), train_set, opt)        
        acc = accuracy(test_set...)    
        if acc >= 0.999
      end
        if acc >= best_acc
        @info("Saving model out to model.bson")
        BSON.@save "model.bson" model epoch_idx acc
        best_acc = acc
        last_improvement = epoch_idx
        end
        if epoch_idx - last_improvement >= 5 && opt.eta > 1e-6
			opt.eta /= 10.0    
        last_improvement = epoch_idx
        end
        if epoch_idx - last_improvement >= 10        
		end
    end
end

# ╔═╡ 47638d66-3f55-4c37-85bc-bd8caea3c73f
begin
	batch_size = 128
    mb_idxs = partition(1:length(train_imgs), batch_size)
    train_set = [make_minibatch(train_imgs, train_labels, i) for i in mb_idxs]
end

# ╔═╡ 3e11a7cc-c136-4c53-9d83-c40982f05a6e
begin
	train_set = gpu.(train_set)
    test_set = gpu.(test_set)
    modl = gpu(model)
end

# ╔═╡ 885bf43e-eb6b-4c7c-ab2d-c393b4baaf35
begin
	test_set = make_minibatch(test_imgs, test_labels, 1:length(test_imgs))
	typeof(train_set)
end

# ╔═╡ Cell order:
# ╠═8b1505c8-48d7-48a4-8fa4-dbc16bb7399b
# ╠═879fa0a8-fd2d-40b9-8dd5-6fea12c993d5
# ╠═bd6a26cc-5967-4a35-8517-6aaa704bcda6
# ╠═720dd163-60ab-46b2-b8ed-8a75045e8c4b
# ╠═a01ce91c-5474-4345-8cb4-c195be2c56f3
# ╠═dbcfc74f-5268-42a1-a99b-ff6cb9631baf
# ╠═5c7becfb-2f5a-4f2e-b3cd-0f7549402987
# ╠═a3967197-cea6-4f57-b885-3a66db649767
# ╠═faf90ddc-8f7f-4bb1-bc29-66755089369b
# ╠═fca0a38c-b4de-4d38-9f68-fa2fab2dccf8
# ╠═635e7dc6-58eb-49e7-b04d-488fa77fc7b0
# ╠═a26c4974-f6bf-43b9-a935-7081a8009626
# ╠═c9cea1a4-0702-4626-a040-b74c70a6704b
# ╠═b2c08ab8-2c05-4d9d-8c6d-f46a1e0bbc78
# ╠═258bbe86-288b-46ce-b80f-3e5ea43c26c6
# ╠═9259deac-3d59-43c2-9942-bf66f046c301
# ╠═82aaca93-7bc2-4975-9812-792792a5dbc9
# ╠═e4945877-3e55-4fa4-a3b8-c03b2301d66a
# ╠═76c96c8d-6a1c-4932-b2ed-1b3138320c77
# ╠═456fd5d0-6516-45a2-8640-49bce711cca2
# ╠═ebf31409-e3c4-402a-b447-aa96683a9e83
# ╠═3625f053-4498-4c0d-9241-5d824bb06044
# ╠═141c330d-fa0c-481f-a111-916c529cd3af
# ╠═2e5f645b-977f-477b-a0fa-7c850202a45d
# ╠═2437042e-a8ee-44d1-a422-70cf053dae3f
# ╠═36bf8b93-124b-42b1-ac19-3b5f0bd4ec7d
# ╠═ac9a7a7b-1b79-47f0-bc10-10d795fb6a7f
# ╠═f9ce70ba-760f-4bc1-8590-da92ebd9c7a4
# ╠═8a90a9ab-2b00-4b6c-8ecb-27b7065484c5
# ╠═f22801f2-29e3-45ab-bc49-381ac2baa7c3
# ╠═4d9a80d2-5226-4001-9de1-51b16fd3c47d
# ╠═26269127-0100-48bb-a0bb-63f9c2e8a20c
# ╠═a9753dc0-d195-4fb6-b90a-4bda5071c8d1
# ╠═29614b02-f1eb-418a-8a39-2ad4f713dfa7
# ╠═4166ad83-f0af-446b-8631-7ba7aea93876
# ╠═6e1add3f-73ce-42d1-aa5b-be0eaa00140b
# ╠═cc632f9b-09ac-432c-8857-ddaa360c571a
# ╠═132865f6-5f64-4b43-a837-58d4b6f0b9ab
# ╠═c0e1e252-354d-4085-9972-c7b46a3271e4
# ╠═26d3e0d5-65b7-4c3d-98ac-7f5f800b9ceb
# ╠═cf5de1ac-c42c-425c-b6d9-abee5236f531
# ╠═d4a9f0d1-19cf-4028-82d4-5c9ddece714e
# ╠═c1ba2839-08e6-45bb-ad40-cda70ebcb601
# ╠═657346a0-5650-4031-b6f6-da4aafeb43df
# ╠═320dc020-61e9-45eb-9993-7080a206196f
# ╠═96e42750-7cbf-477c-8041-0d64ab6c63f7
# ╠═2076fcc1-d5d7-4830-befb-46ccbe13b8c2
# ╠═421063fe-0e8c-4ab5-a500-f1852b5a7bc7
# ╠═ba5d297e-da24-477c-8c20-1f3c9283934e
# ╠═dea0fd8e-5819-4ac7-9bb0-fcdcf5b9f006
# ╠═5bf4a473-3291-479a-943a-db5df2ad5a63
# ╠═453bcea6-5715-42a7-b943-27164f61c9b6
# ╠═f5776823-9ff5-469f-b4a3-0e090b023966
# ╠═bbd25a8f-4dcc-4615-a230-8c93c70e0746
# ╠═bad045e3-08ee-44d0-9f5a-f43839485365
# ╠═988e000a-3ab8-4095-8854-a13071d0e0bf
# ╠═f23a69b8-0922-42dc-91e0-e0190b8e72b8
# ╠═8f444ef5-4ef1-4efd-a2df-7b33b86e8650
# ╠═968826ae-3c59-4950-9d50-ab25ad275ff6
# ╠═e520135c-0b04-473a-a42a-90817086f30d
# ╠═465d03fc-14d7-4d1d-8e17-38637df4656e
# ╠═f665b4c1-b6fb-4119-a560-2799a27a3138
# ╠═1831407d-0ad3-4b68-a58b-7f0297e66dfc
# ╠═1a397da2-0994-40c3-a795-a7268c750251
# ╠═3e11a7cc-c136-4c53-9d83-c40982f05a6e
# ╠═885bf43e-eb6b-4c7c-ab2d-c393b4baaf35
# ╠═47638d66-3f55-4c37-85bc-bd8caea3c73f
